from cotv_backend import app, db
from cotv_backend.videos.main import videos_ctrl
from cotv_backend.models import Video
import datetime
from flask import Flask, render_template, redirect
from sqlalchemy  import  extract

db.create_all()

# app.register_blueprint(videos_ctrl, url_prefix='/videos')

@app.route('/')
def show_all():
    videos = Video.query.limit(40).all()

    cur_prev = "/{}/{}".format(2022, 6-1)
    cur_prox = "/{}/{}".format(2022, 6+1)

    return render_template('page_index.html', videos=videos, prev=cur_prev, prox=cur_prox, date="Junho de 2022" )

@app.route('/<year_str>')
def show_year(year_str):
    year = int(year_str)
    month = 1
    if year <= 2008:
        year = 2008
        month = 6
    return redirect("/{}/{}".format(year, month))


@app.route('/<year_str>/<month_str>')
def show_month(year_str, month_str):
    year = int(year_str)
    month = int(month_str)
    if month > 12:
        return redirect("/{}/01".format(year+1))
    if month == 0:
        return redirect("/{}/12".format(year-1))

    if year < 2008:
        return redirect("/2008/06")
    if year == 2022 and month > 6:
        return redirect("/2022/06")

    videos = Video.query.filter( extract('month',Video.date) == month ).filter( extract('year',Video.date) == year ).limit(200).all()

    month_title = ["", "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"]
    date_str =  "{} de {}".format(month_title[month], year)

    cur_prev = "/{}/{}".format(year, month-1)
    cur_prox = "/{}/{}".format(year, month+1)

    return render_template('page_index.html', videos = videos, prev=cur_prev, prox=cur_prox, date=date_str )


@app.route('/videos/<iid>')
def show_video(iid):
    videos = Video.query.filter_by(youtube_id=iid)
    return render_template('page_show.html', item = videos[0] )



app.run()


exit(0)
import json

fd = open("dados.txt")
raw = fd.read()
fd.close()

data = json.loads(raw)
# print(data)


cont = 0;
for item in data:
    yt_id = item["URL"].split("=")[1]
    date = datetime.strptime(item["date"], '%Y%m%d')
    video = Video(item["title"], yt_id, date)
    db.session.add(video)
    db.session.commit()
    cont += 1
    print(cont)
