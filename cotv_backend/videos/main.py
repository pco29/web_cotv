from flask import Blueprint, render_template

from cotv_backend.models import db

videos_ctrl = Blueprint('videos', __name__)

@videos_ctrl.route('/')
def index():
    return render_template('page_index.html')
